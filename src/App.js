import React from 'react';
import './App.css';

class App extends React.Component {
	state = {
		data: [],
		datalist: [
			{
				image: "https://png.pngtree.com/svg/20170602/avatar_107646.png",
				label: "Hello 1",
				email: "a@b.com",
			}, {
				image: "https://png.pngtree.com/svg/20170602/avatar_107646.png",
				label: "Hello 2",
				email: "a@b.com",
			}, {
				image: "https://png.pngtree.com/svg/20170602/avatar_107646.png",
				label: "Hello 3",
				email: "a@b.com",
			}, {
				image: "https://png.pngtree.com/svg/20170602/avatar_107646.png",
				label: "Hello 4",
				email: "a@b.com",
			}, {
				image: "https://png.pngtree.com/svg/20170602/avatar_107646.png",
				label: "Hello 5",
				email: "a@b.com",
			}, {
				image: "https://png.pngtree.com/svg/20170602/avatar_107646.png",
				label: "Hello 6",
				email: "a@b.com",
			}
		],
		text: "",
	}
	addTag = text => {
		let { data, datalist } = this.state;
		data.push({ label: text, image: "https://png.pngtree.com/svg/20170602/avatar_107646.png", email: "a@b.com" });
		let isDatalist = datalist.find(a => new RegExp(text, "i").test(a.label));
		if (isDatalist) {
			for (const i in datalist) {
				if (datalist[i].label == text) {
					datalist.splice(i, 1)
				}
			}
		}
		this.setState({
			data: data,
			text: "",
			datalist: datalist
		})
	}
	onDelete = index => {
		let { data, datalist } = this.state;
		datalist.push(data[index])
		data.splice(index, 1)
		this.setState({ data: data, datalist: datalist })
	}
	onInputKeyDown = event => {
		if (event.key === 'Enter') {
			this.addTag(this.state.text)
		}
	}
	handleOnChange = event => {
		let { value } = event.target;
		let isDatalist = this.state.datalist.find(a => new RegExp(value.split(" - ")[0], "i").test(a.label));
		if (isDatalist) {
			this.addTag(value.split(" - ")[0])
		} else {
			this.setState({ text: event.target.value })
		}
	}
	render() {
		return (
			<div className="App">
				<div id="tags">
					{this.state.data.map((a, i) => {
						return <div key={i} className="tag_container">
							<div className="tag_image">
								<img src={a.image} width={30} />
							</div>
							<div className="tag_text">
								{a.label}
							</div>
							<div onClick={() => this.onDelete(i)} className="tag_cross">X</div>
						</div>
					})}
					<span className="custom_input">
						<input list="browsers" type="text" value={this.state.text} onChange={this.handleOnChange} onKeyDown={this.onInputKeyDown} placeholder="Add Here" />
						<datalist id="browsers">
							{this.state.datalist.map((a, i) => {
								return <option key={i}>{a.label} - {a.email}</option>
							})}
						</datalist>
					</span>
				</div>

			</div>
		);
	}
}

export default App;
